//
//  ParayerTimes.swift
//  Runner
//
//  Created by OSX on 12/21/19.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

import UIKit
import Adhan
import CoreLocation

class ParayerTimes: UIViewController, UITableViewDataSource {
   
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dateLbl: UILabel!

    var localTimeZoneIdentifier: String { return TimeZone.current.identifier }

    var currentLang = "en"{
        didSet{
            self.tableView.reloadData()
        }
    } //default
   
    var currentLocStr: String!
    var latitude: Double!
    var longitude: Double!
    
    var salwatEng = [
        "fajr",
        "sunrise",
        "dhuhr",
        "asr",
        "maghrib",
        "isha"
    ]
    
    var salwatAr = [
        "الفجر",
        "الصبح",
        "الظهر",
        "العصر",
        "المغرب",
        "العشاء"
    ]
    
    var mwa3edEng = [String]()
    var theDate = Date()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let formatter1 = DateFormatter()
        formatter1.timeStyle = .none
        formatter1.dateStyle = .long
        
        formatter1.locale = Locale(identifier: currentLang)
        
        dateLbl.text =  "\(formatter1.string(from: theDate))"
        
        
        
        if (currentLocStr != nil) {
            //break string to long and lat
            let result = currentLocStr.split(separator: ",")
            self.latitude = Double(result[0])
            self.longitude = Double(result[1])
            
            /*let location = CLLocation(latitude: self.latitude, longitude: self.longitude)
            let geoCoder = CLGeocoder()
            geoCoder.reverseGeocodeLocation(location) { (placemarks, err) in
                if let placemark = placemarks?[0] {
                    print(placemark.timeZone)
                    print(placemark.country)
                }
            }*/
            
            loadPrayers()
        }
        else{
      
            let alert = UIAlertController(title: "Alert", message: "You have to change Quran app access to location to yes in your sttings", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { (_) in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    
    func loadPrayers(){
        
        mwa3edEng = [String]() //clear the saved data
        
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        let date = cal.dateComponents([.year, .month, .day], from: theDate)
        
        let coordinates = Coordinates(latitude: self.latitude!, longitude: self.longitude!)
        
        var params: CalculationParameters!
        let result = localTimeZoneIdentifier.split(separator: "/")

        if (result[0] == "Africa"){
            params = CalculationMethod.egyptian.params
        }
        else {
            // any thing in arabian part like riyad
            params = CalculationMethod.ummAlQura.params
        }
        
        params.madhab = .shafi

        
        if let prayers = PrayerTimes(coordinates: coordinates, date: date, calculationParameters: params) {
            
            let formatter = DateFormatter()
            formatter.timeStyle = .short
            formatter.timeZone = TimeZone(identifier: localTimeZoneIdentifier)!
            formatter.locale = Locale(identifier: currentLang)
            
            mwa3edEng.append("\(formatter.string(from: prayers.fajr))")
            mwa3edEng.append("\(formatter.string(from: prayers.sunrise))")
            mwa3edEng.append("\(formatter.string(from: prayers.dhuhr))")
            mwa3edEng.append("\(formatter.string(from: prayers.asr))")
            mwa3edEng.append("\(formatter.string(from: prayers.maghrib))")
            mwa3edEng.append("\(formatter.string(from: prayers.isha))")
 
            tableView.reloadData()
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return mwa3edEng.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PrayerCell"){
            
            if currentLang == "en"{
                cell.textLabel?.text = self.salwatEng[indexPath.row]
                cell.detailTextLabel?.text = mwa3edEng[indexPath.row]
            }else{
                cell.textLabel?.text = mwa3edEng[indexPath.row]
                cell.detailTextLabel?.text = self.salwatAr[indexPath.row]
            }
            
            return cell
        }
        else{
            return UITableViewCell()
        }
        
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dateBackward(_ sender: Any) {
        
        
        theDate = Calendar.current.date(byAdding: .day, value: -1, to: theDate)!
        
        let formatter1 = DateFormatter()
        formatter1.timeStyle = .none
        formatter1.dateStyle = .long
        formatter1.locale = Locale(identifier: currentLang)
        dateLbl.text = "\(formatter1.string(from: theDate))"
        
        
        loadPrayers()
        
    }
    
    @IBAction func dateForward(_ sender: Any) {
        
        theDate = Calendar.current.date(byAdding: .day, value: 1, to: theDate)!
        
        let formatter1 = DateFormatter()
        formatter1.timeStyle = .none
        formatter1.dateStyle = .long
        formatter1.locale = Locale(identifier: currentLang)
        dateLbl.text = "\(formatter1.string(from: theDate))"
        
        
        loadPrayers()
        
    }
    
    
}
