import UIKit
import Flutter
import GoogleMaps

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    
    
     var flutter_native_splash = 1
     UIApplication.shared.isStatusBarHidden = false
     GMSServices.provideAPIKey("AIzaSyAtuQ9DbGTfz81j288BkUGRsNxrKu7zSuo")
 
    GeneratedPluginRegistrant.register(with: self)
    
    // iOS - AppDelegate.swift
    let channelName = "wingquest.stablekernel.io/speech"
    let rootViewController : FlutterViewController = window?.rootViewController as! FlutterViewController
    
    let methodChannel = FlutterMethodChannel(name: channelName, binaryMessenger: rootViewController as! FlutterBinaryMessenger)
    
    methodChannel.setMethodCallHandler {(call: FlutterMethodCall, result: FlutterResult) -> Void in
        if (call.method == "start") {
            
            if let window = UIApplication.shared.keyWindow {
                guard let rootViewController = window.rootViewController else {
                    return
                }
                
                let mainStory = UIStoryboard(name: "Main", bundle: nil)
                let parayerTimesVC = mainStory.instantiateViewController(withIdentifier: "ParayerTimes") as! ParayerTimes
                parayerTimesVC.view.frame = rootViewController.view.frame
                parayerTimesVC.view.layoutIfNeeded()
                parayerTimesVC.modalPresentationStyle = .overFullScreen
                parayerTimesVC.modalTransitionStyle = .crossDissolve
                
                let arr = call.arguments as! [String]
                parayerTimesVC.currentLocStr = arr[0]
                parayerTimesVC.currentLang = arr[1]
             
                rootViewController.present(parayerTimesVC, animated: true, completion: nil)
                
            }
        }
    }
    
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
